package com.dummy.myerp.business.integration;

import com.dummy.myerp.business.util.Constant;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.assertj.core.api.Assertions;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:bootstrapContext.xml")

@Sql({ "classpath:sql/01_create_schema.sql",
                "classpath:sql/02_create_tables.sql",
                "classpath:sql/21_insert_data_demo.sql" })

@Transactional(propagation = Propagation.REQUIRED)
public class TestBusinessIntegration extends BusinessTestCase {

        private EcritureComptable sampleEcritureComptable;

        public TestBusinessIntegration() {
                super();
        }

        @Test
        public void getListCompteComptable() {
                List<CompteComptable> compteComptableList = getBusinessProxy().getComptabiliteManager()
                                .getListCompteComptable();

                Assert.assertEquals(7, compteComptableList.size());
        }

        @Test
        public void getListJournalComptable() {
                List<JournalComptable> journalComptableList = getBusinessProxy().getComptabiliteManager()
                                .getListJournalComptable();
                Assert.assertEquals(4, journalComptableList.size());
        }

        @Test
        public void getListEcritureComptable() {
                List<EcritureComptable> ecritureComptableList = getBusinessProxy().getComptabiliteManager()
                                .getListEcritureComptable();
                for (EcritureComptable ecritureComptable : ecritureComptableList) {
                }
                Assert.assertEquals(5, ecritureComptableList.size());
        }

        @Before
        public void init() {

                sampleEcritureComptable = new EcritureComptable();
                sampleEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
                sampleEcritureComptable.setDate(new Date());
                sampleEcritureComptable.setLibelle("Libelle");
                sampleEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                sampleEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));
        }

        /**
         * Ajoute une nouvelle sequence
         * lorsque la reference n'existe pas
         * 
         * @throws NotFoundException
         * @throws FunctionalException
         */
        @Test
        public void addReference_shouldcreateNewSequenceInDB_whenSequenceNotFound()
                        throws NotFoundException, FunctionalException {

                // add Reference
                SpringRegistry.getBusinessProxy().getComptabiliteManager().addReference(sampleEcritureComptable);
                Assert.assertEquals("AC-2022/00001", sampleEcritureComptable.getReference());

                // checkEcritureComptable
                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .checkEcritureComptable(sampleEcritureComptable);
        }

        @Test
        public void addReference_shouldUpdateReference_whenRefExisting() throws NotFoundException, FunctionalException {

                // add Reference
                SpringRegistry.getBusinessProxy().getComptabiliteManager().addReference(
                                sampleEcritureComptable);
                Assert.assertEquals("AC-2022/00001", sampleEcritureComptable.getReference());

                // insert ecriture
                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .insertEcritureComptable(sampleEcritureComptable);

                // add reference again

                EcritureComptable eComptable = new EcritureComptable();
                eComptable.setJournal(new JournalComptable("AC", "Achat"));
                eComptable.setDate(new Date());
                eComptable.setLibelle("Libelle");
                eComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                eComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));

                SpringRegistry.getBusinessProxy().getComptabiliteManager().addReference(
                                eComptable);

                // insert ecriture
                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .insertEcritureComptable(eComptable);

                Assert.assertEquals("AC-2022/00002", eComptable.getReference());

        }

        /**
         * Erreur a verifier - la date de l'ecriture n'est pas définie
         * 
         * @throws NotFoundException
         * @throws FunctionalException
         */
        @Test
        public void addReference_noDate_shouldFunctionalException() throws NotFoundException, FunctionalException {
                sampleEcritureComptable.setDate(null);

                Assertions.assertThatThrownBy(() -> SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .addReference(sampleEcritureComptable))
                                .isInstanceOf(FunctionalException.class)
                                .hasMessageContaining(Constant.ECRITURE_COMPTABLE_DATE_NULL_FOR_ADD_REFERENCE);
        }

        @Test
        public void testAddReferenceExistingReference() throws FunctionalException, NotFoundException {
                EcritureComptable vEcritureComptableExisting = null;
                vEcritureComptableExisting = SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .getEcritureComptableById(5);
                Assert.assertEquals("BQ-2016/00002", vEcritureComptableExisting.getReference());

                sampleEcritureComptable.setReference(vEcritureComptableExisting.getReference());
                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .addReference(vEcritureComptableExisting);
                Assert.assertEquals("BQ-2016/00003", vEcritureComptableExisting.getReference());

                // insert
                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .insertEcritureComptable(vEcritureComptableExisting);

                // check
                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .checkEcritureComptable(vEcritureComptableExisting);
        }

        /**
         * RG5 avec le code.
         * 
         */
        @Test
        public void checkEcritureComptable_RG_5_badCode_shouldFunctionalException() throws FunctionalException {
                EcritureComptable vEcritureComptable;
                vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
                vEcritureComptable.setDate(new Date());
                vEcritureComptable.setLibelle("Libelle");
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));

                vEcritureComptable.setReference("BC-2022/00001");

                Assertions.assertThatThrownBy(() -> SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .checkEcritureComptable(vEcritureComptable))
                                .isInstanceOf(FunctionalException.class)
                                .hasMessageContaining(Constant.RG_COMPTA_5_VIOLATION);

        }

        /**
         * RG5 avec la date.
         * 
         */
        @Test
        public void checkEcritureComptable_RG_5_badDate_shouldFunctionalException() throws FunctionalException {
                EcritureComptable vEcritureComptable;
                vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
                vEcritureComptable.setDate(new Date());
                vEcritureComptable.setLibelle("Libelle");
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));

                vEcritureComptable.setReference("AC-2018/00001");
                Assertions.assertThatThrownBy(() -> SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .checkEcritureComptable(vEcritureComptable))
                                .isInstanceOf(FunctionalException.class)
                                .hasMessageContaining(Constant.RG_COMPTA_5_VIOLATION);

        }

        /**
         * RG 6 L'unicité de reference
         */
        @Test
        public void checkEcritureComptable_RG_6() throws FunctionalException {
                EcritureComptable vEcritureComptable;
                vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
                vEcritureComptable.setLibelle("Libelle");
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));

                Calendar calendar = new GregorianCalendar(2016, 0, 25);
                vEcritureComptable.setDate(calendar.getTime());

                vEcritureComptable.setReference("AC-2016/00001");

                Assertions.assertThatThrownBy(() -> SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .checkEcritureComptable(vEcritureComptable))
                                .isInstanceOf(FunctionalException.class)
                                .hasMessageContaining(Constant.RG_COMPTA_6_VIOLATION);
        }

        /**
         * Insert
         * On verifie l'insertion d'une nouvelle ecriture comptable
         * 
         * @throws FunctionalException
         */
        @Test
        public void insertEcritureComptable() throws FunctionalException, NotFoundException {

                List<EcritureComptable> listEcritureComptables = getBusinessProxy().getComptabiliteManager()
                                .getListEcritureComptable();
                Assert.assertEquals(listEcritureComptables.size(), 5);

                EcritureComptable vEcritureComptable;
                vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
                vEcritureComptable.setLibelle("Libelle");
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));

                Calendar calendar = new GregorianCalendar(2022, 0, 15);
                vEcritureComptable.setDate(calendar.getTime());
                vEcritureComptable.setReference("AC-2022/00001");

                SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .insertEcritureComptable(vEcritureComptable);

                listEcritureComptables = getBusinessProxy().getComptabiliteManager()
                                .getListEcritureComptable();

                Assert.assertEquals(listEcritureComptables.size(), 6);

        }

        /**
         * Insert
         * On vérifie que la date de l'écriture corresponde bien avec la référence lors
         * de l'insertion.
         */
        @Test
        public void insertEcritureComptable_DateError_RG5() throws FunctionalException {
                EcritureComptable vEcritureComptable;
                vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
                vEcritureComptable.setDate(new Date());
                vEcritureComptable.setLibelle("Libelle");
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, new BigDecimal(123),
                                null));
                vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                null, null,
                                new BigDecimal(123)));
                vEcritureComptable.setReference("AC-2016/00002");
                Assertions.assertThatThrownBy(() -> SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .insertEcritureComptable(vEcritureComptable))
                                .isInstanceOf(FunctionalException.class)
                                .hasMessageContaining(Constant.RG_COMPTA_5_VIOLATION);
        }

        /**
         * Update
         * On verifie que la modification de l'ecriture
         */
        @Test
        public void updateEcritureComptable() throws FunctionalException, NotFoundException {
                EcritureComptable ecritureComptable = SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .getEcritureComptableById(1);

                Assert.assertEquals(ecritureComptable.getListLigneEcriture().size(), 3);

                ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                "Nouvel Achat ", new BigDecimal(123),
                                null));
                ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                                "Nouvel Achat ", null,
                                new BigDecimal(123)));

                ecritureComptable.setReference("AC-2016/00002");

                SpringRegistry.getBusinessProxy().getComptabiliteManager().updateEcritureComptable(ecritureComptable);

                Assert.assertEquals(ecritureComptable.getListLigneEcriture().size(), 5);

        }

        /**
         * Delete
         * On vérifie la suppression de l'ecriture comptable
         */

        @Test
        public void testDeleteEcritureComptable() throws NotFoundException {
                EcritureComptable vEcritureComptableExisting = null;
                vEcritureComptableExisting = SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .getEcritureComptableById(3);

                getBusinessProxy().getComptabiliteManager().deleteEcritureComptable(3);

                Assertions.assertThatThrownBy(() -> SpringRegistry.getBusinessProxy().getComptabiliteManager()
                                .getEcritureComptableById(3))
                                .isInstanceOf(NotFoundException.class);
        }

}
