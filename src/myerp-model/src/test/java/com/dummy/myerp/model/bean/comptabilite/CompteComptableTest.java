package com.dummy.myerp.model.bean.comptabilite;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class CompteComptableTest {

    private CompteComptable compteComptable;

    private CompteComptable compteComptable2;

    private CompteComptable compteComptable3;

    @Test
    public void getByNumero() {
        compteComptable = new CompteComptable(1, "first");
        compteComptable2 = new CompteComptable(2, "second");

        List<CompteComptable> list = new ArrayList<>();
        list.add(compteComptable);
        list.add(compteComptable2);

        compteComptable3 = CompteComptable.getByNumero(list, 2);

        Assert.assertEquals(compteComptable3.getLibelle(), compteComptable2.getLibelle());

    }
}
